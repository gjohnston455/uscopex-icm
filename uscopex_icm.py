import base64
import numpy as np 
import io
import os
from PIL import Image 
import keras
from keras.utils import CustomObjectScope
from keras.initializers import glorot_uniform
from keras import backend as K 
from keras.models import Sequential
from keras.models import load_model
from keras.preprocessing.image import ImageDataGenerator
from keras.preprocessing.image import img_to_array
from flask import request
from flask import jsonify
import flask
import werkzeug
import tensorflow as tf
import pymysql
from ftplib import FTP 
import datetime



app = flask.Flask(__name__)

def dbInsert(classification_name,model_id,Description,Classification,User_ID):
	conn = pymysql.connect(host='uscopex-1.cieqhbfcg2z7.us-east-1.rds.amazonaws.com', user='admin', passwd='University1', db='uscopex')
	conn.autocommit(True)
	now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
	cur = conn.cursor()

	try:
		cur.execute("INSERT INTO ICM_Result(Name,Description,Classification,Model,Date,User_ID) VALUES (%s,%s,%s,%s,%s,%s)",(classification_name,Description,Classification,model_id,now,User_ID))
	except IntegrityError as e:
		print ('Mysql error,%s'%e)

	cur.close()
	conn.close()


def downloadModel(model_name,uniqueModelName):

	print (" * downoading Keras model ...",uniqueModelName)

	ftp = FTP('gjohnston32.lampt.eeecs.qub.ac.uk')
	ftp.login(user='gjohnston32',passwd='0C6Z8kvP7TlgtyMh')
	ftp.cwd('/httpdocs/uscopex_web/icm_model')
	localfile = open(uniqueModelName, 'wb')
	ftp.retrbinary('RETR ' + model_name, localfile.write, 1024)
	ftp.quit()
	localfile.close()
	
def loadModel(model_to_load,uniqueModelName):
	global model
	try:
		downloadModel(model_to_load,uniqueModelName)
	except socket.error as e: 
		print ('unable to connect!,%s'%e)

	model = tf.keras.models.load_model(uniqueModelName)
	print(" * Model loaded!")


def resourceCleanUp(uniqueModelName,uniqueImageName):
	print(" Delteing file ")
	os.remove(uniqueModelName)
	print(" Delteing image ")
	os.remove(uniqueImageName)
	

def preprocessing(path):
	img = Image.open(path)
	img = img.resize((32,32))
	img = np.array(img)
	img = img / 255.0
	img = img.reshape(1,32,32,3)
	return img

def predict(filename):
	preprocessedImage = preprocessing(filename) 
	prediction = np.argmax(model.predict(preprocessedImage), axis=-1)
	return prediction

@app.route('/classify', methods = ['GET', 'POST'])
def handle_request():
	now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
	imagefile = flask.request.files['imagefile']
	model_name = request.form['model_name']
	classification_name = request.form['classification_name']
	Description = request.form['description']
	User_ID = request.form['user_id']
	model_Id = request.form['model_id']
	uniqueModelName = User_ID +"_"+ now +"_"+  model_name 
	uniqueImageName = User_ID +"_"+ now +"_"+  imagefile.filename
	model_to_load = model_name +".h5"
	
	print(model_to_load)
		
		
	filename = werkzeug.utils.secure_filename(imagefile.filename)
	print("\nReceived image File name : " + imagefile.filename)
	imagefile.save(uniqueImageName)


	print (" * Loading Keras model ...")
	loadModel(model_to_load,uniqueModelName)

	print (" * Prediction  ...")
	prediction = int (predict(uniqueImageName))
	print(prediction)

	resourceCleanUp(uniqueModelName,uniqueImageName)
	
	dbInsert(classification_name,model_Id,Description,prediction,User_ID)
	return jsonify(
		error="false",
		message= prediction,
		code= 200
		)

app.run(host="0.0.0.0", port=5000,threaded=True, debug=True)


